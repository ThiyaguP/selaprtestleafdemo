package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLeadPage extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Create Lead in leaftaps";
		author = "Thiyagu P";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void CreateLeads(String uName, String pwd,String cName,String fName,String lName,String phNumber) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickOnCRMlink()
		.clickOnLeadsTab()
		.clickOnCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.enterphNumber(phNumber)
		.clickOnCreateLeadButton();
		//.clickLogout();
		
	}
	
}






