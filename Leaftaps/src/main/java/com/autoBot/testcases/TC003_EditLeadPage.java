package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC003_EditLeadPage extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Create Lead in leaftaps";
		author = "Thiyagu P";
		category = "smoke";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void EditLeads(String uName, String pwd,String phNumber,String lfName,String llName,String dept) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickOnCRMlink()
		.clickOnLeadsTab()
		.clickOnFindLead()
		.clickOnPhone()
		.enterPhoneNumber(phNumber)
		.clickOnFindLeadButton()
		.clickOnLeadId()
		.clickOnEditButton()
		.enterLocalFirstName(lfName)
		.enterLastLocalName(llName)
		.enterDepartment(dept)
		.clickUpdateButton()
		.clickLogout();
		//.clickLogout();
		
	}
	

}




