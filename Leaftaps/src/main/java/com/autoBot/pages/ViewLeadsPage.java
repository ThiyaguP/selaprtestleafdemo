package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class ViewLeadsPage extends Annotations{ 

	public ViewLeadsPage() {
       PageFactory.initElements(driver, this);
	} 
	
	public EditLeadPage clickOnEditButton() {
		WebElement eleEdit = locateElement("link", "Edit");
		click(eleEdit);  
		return new EditLeadPage();
	}

//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public LoginPage clickLogout() {
		WebElement eleLogout = locateElement("link", "Logout");
		click(eleLogout);  
		return new LoginPage();
	}

}







