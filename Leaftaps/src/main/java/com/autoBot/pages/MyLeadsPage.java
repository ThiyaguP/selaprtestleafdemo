package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations{ 

	public MyLeadsPage() {
		PageFactory.initElements(driver, this); 
	}

	/*public HomePage() {
       PageFactory.initElements(driver, this);
	} 
*/
//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public CreateLeadPage clickOnCreateLead() {
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);  
		return new CreateLeadPage();
	}

	public FindLeadPage clickOnFindLead() {
		WebElement eleFindLead = locateElement("link","Find Leads");
		click(eleFindLead);
		return new FindLeadPage();
	}
}







