package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;

public class FindLeadPage extends Annotations{ 

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	} 

	//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	@Test(dataProvider="fetchData")
	public FindLeadPage clickOnPhone()
	{
		WebElement elePhone = locateElement("xpath","//span[text()='Phone']");
		click(elePhone);
		return this;
	}

	public FindLeadPage enterPhoneNumber(String data)
	{
		WebElement elePhoneNumber = locateElement("xpath","//input[@name='phoneNumber']");
		clearAndType(elePhoneNumber,data);
		return this;
	}

	public FindLeadPage enterfirstName(String data)
	{
		WebElement eleFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		clearAndType(eleFirstName,data);
		return this;
	}
	public FindLeadPage clickOnFindLeadButton() throws InterruptedException
	{
		Thread.sleep(3000);
		WebElement eleFindLeadB = locateElement("xpath","//*[@id='ext-gen334']");
		click(eleFindLeadB);
		return this;
	}
	public ViewLeadsPage clickOnLeadId() throws InterruptedException
	{
		Thread.sleep(3000);
		WebElement eleFindLeadB = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFindLeadB);
		return new ViewLeadsPage();
	}

}









