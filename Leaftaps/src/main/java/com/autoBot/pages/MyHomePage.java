package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations{ 

	public MyHomePage() {
       PageFactory.initElements(driver, this);
	} 

//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public MyLeadsPage clickOnLeadsTab() {
		WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);  
		return new MyLeadsPage();
	}

}







