package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations{ 

	public HomePage() {
       PageFactory.initElements(driver, this);
	} 

//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	
	public MyHomePage clickOnCRMlink() {
		WebElement eleCRM = locateElement("link", "CRM/SFA");
		click(eleCRM);  
		return new MyHomePage();
	}
	
	public LoginPage clickOnLogout() {
		// TODO Auto-generated method stub
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
		return new LoginPage();
	}

}







