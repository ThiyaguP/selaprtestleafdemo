package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;

public class EditLeadPage extends Annotations{ 

	public EditLeadPage() {
       PageFactory.initElements(driver, this);
	} 

//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	@Test(dataProvider="fetchData")
	public EditLeadPage enterLocalFirstName(String data)
	{
		WebElement elelfName = locateElement("id", "updateLeadForm_firstNameLocal");
		clearAndType(elelfName,data);
		return this;
	}
	public EditLeadPage enterLastLocalName(String data)
	{
		WebElement elellName = locateElement("id", "updateLeadForm_lastNameLocal");
		clearAndType(elellName,data);
		return this;
	}
	public EditLeadPage enterDepartment(String data)
	{
		WebElement eledept = locateElement("id", "updateLeadForm_departmentName");
		clearAndType(eledept,data);
		return this;
	}
		
	public ViewLeadsPage clickUpdateButton() {
		// TODO Auto-generated method stub
		WebElement eleUpdate = locateElement("Name", "submitButton");
		click(eleUpdate);  
		return new ViewLeadsPage();
	}

}







