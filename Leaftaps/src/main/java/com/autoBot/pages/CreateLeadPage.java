package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	} 

//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	@Test(dataProvider="fetchData")
	public CreateLeadPage enterCompanyName(String data)
	{
		WebElement elecName = locateElement("id", "createLeadForm_companyName");
		clearAndType(elecName,data);
		return this;
	}
	public CreateLeadPage enterFirstName(String data)
	{
		WebElement elefName = locateElement("id", "createLeadForm_firstName");
		clearAndType(elefName,data);
		return this;
	}
	public CreateLeadPage enterLastName(String data)
	{
		WebElement elelName = locateElement("id", "createLeadForm_lastName");
		clearAndType(elelName,data);
		return this;
	}
	public CreateLeadPage enterphNumber(String data)
	{
		WebElement elephNumber = locateElement("id", "createLeadForm_primaryPhoneNumber");
		clearAndType(elephNumber,data);
		return this;
	}
	
	public ViewLeadsPage clickOnCreateLeadButton() {
		// TODO Auto-generated method stub
		WebElement eleCreateLead = locateElement("Name", "submitButton");
		click(eleCreateLead);  
		return new ViewLeadsPage();
	}

}







